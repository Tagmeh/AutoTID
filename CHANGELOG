
#5/4/2022
Version 3.0.3
# Updated the Username Prompt to explain that this does not actually change your password in the systems
# Revised the passwordRegex to include a trailing colon ":"
# Switched which username is used in telnet (from network name to cuid)

#5/3/2022
Version 3.0.2
# Added an "Issues" section to the README

Version 3.0.1
# Fixed an issue with updating the cuid via the script.
# Increased passwordLoop from 500ms to 750ms

Version 3.0.0
# Added support for the newly migrated Orange devices (ssh cuid@tid)
# Added new required data to config and setup prompts (cuid and cuid password)
# Added commands to update the cuid and cuid password independently ("set cuid" and "set cuid password")

#7/25/2019
Version 2.1.11
#Fixed issue where ring val doesn't continue after failure to log into orange TID.

#3/4/2019
Version 2.1.5
#Added a timeout when telnetting into an Orange box. (4 seconds)
#Ring Val now skips refused/failed telnet connections w/ a note on the summary.
#The Config command can now be used to access any of the Go Forward NID products on the Config-Services.pl script (in geofront)
#Created the "Version" command. It'll tell you your current version, along with if there's a newer version out.
#Moved some global objects related to the Ring Val, into the Ring Val function.

#2/25/2019
Version 2.1.4
#Fixed an error when the ring val tool got stuck trying to log into an unresponsive device.

#2/22/2019
Version 2.1.3
#Fixed a bug where geofront was always the orange bastion server, even if the settings showed a different orange bastion

#2/21/2019
Version 2.1.2
#Removed a forgotten troubleshooting popup
#Fixed an error when logging into an already-configured Red ADVA for the first time.
#Setting the "Testing" option to 1 also enables the "RenameTab" option.
#Changed up the custom error messages.
#Fixed an incorrect function call in PasswordLoop, affecting random devices upon login.

#2/20/2019
Version 2.1.0
#Fixed bug where an error message would popup if no user input was detected on close.
#Fixed Central NID issues.
#Removed Ring Val only option.
#Significantly increased the script speed, over version 2.0.0.
#Fixed all of the broken things from 2.0.0 (which was most things).
#Revamped most of the regexes to be more specific.
#Metro Ring search is working again (thanks regexes!)

#2/16/2019
Version 2.0.0
#Rebuilt AutoTID, streamlined the code
#Removed the Terminal Monitor sections
#Added navigating to the Red or Orange bastion specifically
#Ring Val now runs through the ring in order as it appears with "show rep topology"

#1/6/2019
Version 1.6.18
#Changed the Build:x/x to be less confusing. For the Ring Val tool.

#1/3/2019
Version 1.6.17
#Adjusted the script to accommodate users with a different Orange Bastion prompt

#12/26/2018
Version 1.6.15
#Disabled the ring node search due to bugs
#Added a hacked-together ring builder (only initializes the vlan, doesn't built interfaces)
#Added the live ring val update in-tab
#Fixed a bug where the script would get stuck when searching for a CID or CLLI in the same tab
#AutoTID now works with blue Accedians, but only on ESPs
#Removed the MAC section from Ring Val

#12/6/2018
Version 1.6.13
#Reformatted code to make it easier to update

#12/6/2018
Version 1.6.12
#Fixed some grammatical  errors. 
#removed "set" from the commands in the "help" prompt.
#Added the ability to search for Metro nodes
#Added the ability to search for route targets
#Fixed the ring val man popup
#Added some measures to prevent the double telnet prompt issue. Unable to replicate issue.

#11/29/2018
Version 1.6.11
#Fixed a couple of bugs
#No longer need "set" to change password or bastion server.
#Fixed Ring Val exit/logout issue with starting tab
#Added telnet support for ADVA 825
#Fixed where a blue regex was pulled out from ALPR1325W2001 and the ring val fails
#Fixed default bastion selection
#Cleaned up the Ring Val section. Should be more reliable.


#10/23/2018
Version 1.6.10
#Added a delay after printing the last line of the Ring Val output.
#Ring Val Man should no longer pull in non ring node devices.
#Fixed a bug where Ring Val wasn't opening in a new tab.
#Fixed a bug where Ring Val was timing out and then pasting a bunch of stuff.
#Fixed a bug where the strTID wasn't being stripped of all whitespace/_0/)
#Added a regex to remove non-whitespace characters from StrTID.
#Fixed Terminal Monitor (not like anyone uses it anyways...)
#Changed TermMoni's variable to be easier to understand.
#Fixed the bastion error after logging into the device.

#10/21/2018
Version 1.6.9
#Increased the wait period when logging out of devices. Geofront was especially slow one morning. Bandaid.
#Refactored a ton of code, So much code!
#Added ability to search Metro rings
#Added open-in-new-tab options for the individual subprograms
#Added a global open-in-new-tab option
#Added a check for a logon script, if found, all subprograms run in the same tab they're called from.
#Changed AutoTID auto-timeout to 20 seconds, added more catches.
#Ring Val now skips devices that take longer than 10 seconds to respond.
#Ring Val now has an option to let the user manually drop a list of TIDs to validate.
    New command "Ring Val Man" as in Manual or Manually.
#Ring Val can now log all interfaces in a bridge domain on ZG and ZF devices.
#Ring Val now shows a count of how many active mac address are on the searched vlan
    Does not yet log the actual macs. Verbose mode is coming in the future.
#Fixed a bunch of bugs
#Introduced a few bugs
    That's called progress!


#10/16/2018
Version 1.6.8
#Updated the "help" command to include the CID and CLLI functionality.
#LoadAutoTIDConfigSettings > LoadAutoTIDConfigSettingsFromFile
#GetDestinationTID > PromptUserForDestinationTID
#Hotfix for the ring validator and sessions using login scripts

#10/11/2018
Version 1.6.7
#Refactored some code
#Added CLLI search
#Fixed GES exit issue
#Made the tid box more forgiving with _0 and _N/A. 
#Set the default bastion server to Geofront.

Version 1.6.4
#Fixed a bug where the script continued to run after logging into a GES device.

#8/9/2018
Version 1.6.2
#Fixed most of the bugs iwth the Ring Validator
#Added a summary output for Ring Validator

#8/9/2018
Version 1.6.1
#fixed an issue where the script hung if it couldn't reach an ADVA

#8/8/2018
Version 1.6.0 
# Added more error catches, to cancel the script
	# bad commands
	# ROCIing into ROCI
	# nocsup is unable to resolve X in the following domains:
# Password no longer updates if the field is left blank
# Added a comprehensive "help" command
# Updated the TID box to encourage other commmands (displays "help" )
# Added the "tm" option to append to orange drop TIDs
# Updated the error prompts with better, more concise information
# Added script time out. It's about 30 seconds into the PasswordLoop()
# Added another telnet catch for nocsup for when nocsup responds: "Access Denied - Unable to determine vendor/type - Please use personal credentials"
# Now prompts for the OTP if ROCI fails but SSH succeeds.

#7/17/18
Version 1.5.9-13
#1.5.13 - Cleaned up forgotten Msgboxes, Added a tabname change while logging into devices (for fun, ya know)
#1.5.12 - Added "config" command to quickly open a session for the Orange ADVA flow config prompt.
#1.5.11 - Added "setup" command, to rerun the initial setup.
#1.5.10 - Fixed issue with double erroring when entering in the wrong TID
#1.5.9 - Fixed issue of double sending "roci red esp" info. Looks more polished and is more reliable.

##7/16/18
Version 1.5.8
#Fixed an issue where AutoTAB wasn't renaming tabs and staying in the loop. Not elegant, maybe revise later.

## 7/15/18
Version 1.5.3-6
#1.5.6 - Login loop now cancels when it sees certain errors. Also created auto-failover for red devices (telnet/ssh)
#1.5.4 - Added option to drop "terminal monitor" into select orange drop nodes upon connection
#1.5.3 - Added "ACC" or "ADVA" to red nids to help differentiate them.

## 7/1/18
Version 1.5.2
#Fixed the bug that caused Main() to run twice after changing default bastion server.

## 7/1/18
Version 1.5.1
#Disabled Orange failover detection. Too many issues atm. Pending fix.
#Fixed a couple of bugs when open-in-a-new-tab is set to yes.

## 7/1/18
Version 1.5.0
#Script now attempts telnet if SSH fails on Orange side
#Dramatically increased the speed and reliablility of logging into any node

## 6/28/18
Version 1.4.2
#Added waitforstrings to account for the Orange bastion servers being slow

## 6/13/18
Version 1.4.1
#Added Gitlab emails to error codes

## 6/13/18
Version 1.4
#Script now logs into ER/ES devices from the Orange ecosystem
#Fixed exiting Accedians
#Fixed logging out of Red ADVAs
#Fixed logging into any NID from the orange ecosystem
#Sped up logging into Accedians by a few seconds. #Optimized!

## 6/13/18
Version 1.3.10
#Added a half second wait when leaving the Orange network

## 6/1/18
Version 1.3.9
#Central NIDs now login using SSH
#Red ADVAs now use root instead of admin
#Accedians should now pass the md5 check (for new logins)

## 5/29/18
Version 1.3.5
#Fixed logging into pre-configured Red ADVAs (admin changed to root)

## 5/24/18
Version 1.3.4
#Added support for configured and unconfigured Red ADVAs

## 5/17/18
Version 1.3.3
#Added unconfigured Accedian support
#Created GES/MES unsaved changes check
#Hopefully fixed database issue


## 5/17/18
Version 1.3.2
#Added Central NID support
#Identified issue with database. Set Password wipes file.
    Working on fix.

## 5/15/18
Version 1.3.1
#Tabs are now stripped out of the TID string


## 5/12/18
Version 1.3.0
# Added support for Red ADVAs (Default login only for now)
# Script no longer accepts blank passwords
# Script no longer accepts default accedian password
# Fixed potential for loop if all 3 orange bastion servers are down
# Clarified Welcome prompt a bit
# Reorganized code
# Updated Index
# Removed unnessecary code (Include())
# Increased code readability via comments (not complete)
# There is no catch for disconnected NIDs (yet)
# There is no catch for which login to use for ADVAs (yet)
# Unable to replicate dictionary errors. Hoping above fixes have solved this.

## 5/9/18
Version 1.2.6
# AutoTIDConfig file is now built using a dictionary
# Added ability to update password from TID prompt
# Added ability to change Orange bastion server from TID prompt
# The Orange bastion server option is now in the AutoTIDConfig file

## 5/8/18
Version 1.2.5
# Fixed vpe logout issue
# Removed an uneessecary function
# Cleaned up spacing
# Added index 

## 5/6/18
Version 1.2.4
# Added support for blue switches and 4C devices.
# Fixed the bug where the script would stop before roci-ing into the red device (coming from the Orange ecosystem).
# Added disclaimer for non-ROCi accounts. 
# Consolidated code for easier readability.
# Added more comments.

## 5/4/18
Version 1.2.2
# Reverted previous change.
# Fixed bug where the script would stop when leaving the orange bastion server. 


## 4/11/18
Version 1.2.0
# Added a setup wizard 
# Passwords are now stored off-script
# Fixed some bugs caused by the new features (1.2.1)


## 4/6/18
# Added option to change Orange bastion server.
# Updated Readme with disclaimer, options example, and known issues.
# Added Version information to script.


## 4/5/18
# Added support for GES and MES devices.
