#$Language="VBScript"
#$Interface="1.0"
'Author = John Epperson
'Email = incoming+WorkCrew/AutoTID@incoming.gitlab.com

'------------------------------------------------------------
'-------------------------Settings---------------------------
'------------------------------------------------------------

' 0 = to Disable, 1 = to Enable.

RenameTab = 1							' Renames the current Tab with the destination TID.

MasterOpeninNewTab = 0					' Forces all other "Open in new Tab" options to "1".
	OpenAutoTIDinNewTab = 0				' Logs into the destination device in a new tab.
	OpenCircuitIDSearchinNewTab = 0		' Circuit ID search in Geofront using Search.pl.
	OpenCLLISearchinNewTab = 0			' CLLI search in Geofront using Search.pl.
	OpenRouteTargetinNewTab = 0			' Route Target search in Geofront using Search.pl
	OpenMetroRingIDinNewTab = 0			' Open Metro Ring search.pl in a new tab.
	OpenRingValinNewTab = 0				' Open Ring Validator in a new tab.
	OpenAdvaConfigGeneratorinNewTab = 0	' Opens the ADVA config script in a new tab. Recommended.
Testing = 0 							' Not dangerous, but might cause the tab to flicker a bit.

'--------------------------------------------------------------------------------
'----------------------------Paste the new script below--------------------------
'--------Then delete the new script down to this point (on the new script)-------
'--------------------------------------------------------------------------------

version = "3.0.3" 

crt.Screen.Synchronous = True

'Global objects and variables
Set objTab = crt.GetScriptTab
Set currentTab = crt.GetActiveTab
Set autoConfigFileObject = CreateObject("Scripting.FileSystemObject")
Set Database = CreateObject("Scripting.Dictionary")
AutoTIDConfig = GetMyDocumentsFolder & "\AutoTIDConfig.txt"
Dim completed

Const ForReading = 1
Const ForWriting = 2
Const ForAppending = 8

If Testing = 1 then
	RenameTab = 1
End If

Function Main()
	Dim orangeUsername, userPass, cuid, cuidPass, redAccedian, redADVA, Bastion, strTID, strBase
	
	If autoConfigFileObject.FileExists(AutoTIDConfig) then
		' AutoTID Config file was found. The settings can now be pulled into the script.
		
		Call ReloadDB(Database, AutoTIDConfig)		'Grabs the AutoTIDConfig file to use the passwords.
		
		If Database("userPass") = "" OR Database("cuid") = "" OR Database("cuidPass") = "" then
			' AutoTid Config file was found, but was an older version, so it's missing some of the newer required fields.
			If RunSetup() = 0 then
				Exit Function
			End If
		End If
		
	Else
		' AutoTID Config file was not found, run the below functions to have the user build the file.
		If RunSetup() = 0 then
			Exit Function
		End If
	End If

	orangeUsername = Database("orangeUsername") ' Orange ecosystem username.
	userPass = Database("userPass")				' Orange ecosystem password.
	redAccedian = Database("redAccedian")		' Red accedian password.
	redADVA = Database("redADVA")				' Red ADVA default password.
	Bastion = Database("bastionServer")			' Default Orange bastion server (Geofront).
	cuid = Database("cuid")						' User CUID username
	cuidPass = Database("cuidPass")				' User CUID password
	
	If Bastion = "" then
		Bastion = 1
	End If
	
	TabHasLogonScript() ' Check the session for any scripts set to run on logon.
	SetMasterOpeninNewTab() ' Forces the other "inNewTab" options to 1 (yes) if the Master option is set to 1
	
	strTID = PromptUserForDestinationTID("AutoTID") ' Prompts user for input, then sanitizes input and spits it back out here.
	If strTID = "" then 'Close script if there's no user entered data.
		Exit Function
	End If
	
	If SelectSubprogram(strTID) = "Null" then ' Checks the user input against a list of subprogams. 
		Exit Function
	End If
		
	destinationTID = destinationTIDType(strTID) ' Get the color of the destination ecosystem based on the user inputed string.
	If destinationTID = "Null" then
		Exit Function ' Exits Main after exiting destinationTIDType
	
	ElseIf destinationTID = "Nocsup" then ' This is needed so the final tab name isn't some partial word like "re" or "oran"
		strTID = "Red"	
	ElseIf destinationTID = "Bastion" then ' This is needed so the final tab name isn't some partial word like "re" or "oran"
		strTID = "Orange"
	End If

	If OpenAutoTIDinNewTab = 0 then	'Runs in current tab
		Call TabName("Logging into " & strTID, currentTab)
		If Testing = 1 then
			Call TabName("ExitorLogout", currentTab)
		End If
		ExitorLogout() ' Logs out of whatever device the user started the script in.
	ElseIf OpenAutoTIDinNewTab = 1 then	'Indicates the OpenAutoTIDinNewTab option is set to "yes"
		Set objTab = crt.GetScriptTab 'grab the current tab (where the script started running)
		Set currentTab = objTab.Clone 'clone the current tab
		currentTab.Screen.WaitForString "-1:"
	End If
	
	If Testing = 1 then
			Call TabName("Main", currentTab)
	End If
	Call RedOrangetoRedOrange(currentTab, strTID, GetCurrentBastion(currentTab), destinationTIDType(strTID))
	Call TabName(strTID, currentTab)
End Function

Function RunSetup():
	SetupPrompt()
	Call SetupWizard(AutoTIDConfig)
	Call ReloadDB(Database, AutoTIDConfig)		'Grabs the AutoTIDConfig file to use the passwords.
	If Database("userPass") = "" OR Database("cuid") = "" OR Database("cuidPass") = "" then
		' These are mandatory for the script to opperate. 
		' userPass is the Orange password
		' cuidPass is the CUID password
		msgBox("Missing a required field ('network password', 'cuid', or 'cuid password'")
		RunSetup = 0
		Exit Function
	End If
	RunSetup = 1
End Function

Function AdvaConfigGenerator(NewTab, strTID)
	Set tab = crt.GetScriptTab
	If NewTab = 1 then
		Set tab = tab.Clone
		Bastion = 1
		RedtoOrange(tab)
		Call PasswordLoop(tab, Database("userPass"), strTID)
	Else
		ExitorLogout()
		Call RedOrangetoRedOrange(tab, strTID, GetCurrentBastion(tab), destinationTIDType(strTID))
	End If
	
	strTID = Replace(UCase(strTID), "CONFIG", "")
	If strTID = "" then
		strTID = "1"
	End If
	timeout = 0
	Do
		tab.Screen.Synchronous = False
		crt.Sleep(500)		' Loop runs every half second
		tab.Screen.Synchronous = True
		timeout = timeout + 1
	
		result = crt.Screen.Get(crt.Screen.CurrentRow, 1, crt.Screen.CurrentRow, crt.Screen.CurrentColumn) 'Grabs the latest line in SecureCRT
		If inStr(result, "$") > 0 then
			Exit Do
		End If
	Loop
		
	Call TabName("Flow Config", tab)
	tab.Screen.Send "/home/storage/scripts/provisioning/Config-Services.pl" & VbCr
	tab.Screen.WaitForString "Answer:"
	tab.Screen.Send "2" & VbCr
	tab.Screen.WaitForString "Answer:"
	tab.Screen.Send "29" & VbCr
	tab.Screen.WaitForString "Answer:"
	tab.Screen.Send "1" & VbCr
	tab.Screen.WaitForString "Answer:"
	tab.Screen.Send strTID & VbCr
	' strTID will be a number at this point. Having stripped the "Config" command.
End Function

Function ChangeDefaultBastionServer(bastion, value)
	Database.Item("bastionServer") = value		'Dictionary updated with new bastion server
	Call SaveDB(Database, AutoTIDConfig)				'New value saved to AutoTIDConfig
	Call crt.Dialog.MessageBox("Orange bastion server set to " & bastion & ".", "Default bastion server changed", 64)
End Function

Function ChangePassword()
	userPass = crt.Dialog.Prompt("Orange Password (Only updates the AutoTID config file. Doesn't update the Orange bastions.)", "Orange Password Setup", "", True)
	if userpass = "" then  ' This prevents the script from saving an empty string as a password if the field is left blank (even when canceling)
		Exit Function
	End If
	Database.Item("userPass") = userPass 		'New password is passed to dictionary
	Call SaveDB(Database, AutoTIDConfig)		'New password is saved to AutoTIDConfig
	crt.Dialog.MessageBox("Password updated.")
End Function

Function ChangeCuidPassword()
	cuidPass = crt.Dialog.Prompt("CUID Password", "CUID Password Setup", "", True)
	if cuidPass = "" then  ' This prevents the script from saving an empty string as a password if the field is left blank (even when canceling)
		Exit Function
	End If
	Database.Item("cuidPass") = cuidPass 		'New password is passed to dictionary
	Call SaveDB(Database, AutoTIDConfig)		'New password is saved to AutoTIDConfig
	crt.Dialog.MessageBox("CUID Password updated.")
End Function

Function ChangeCuid()
	cuid = crt.Dialog.Prompt("CUID: (THIS IS CASE SENSITIVE)", "CUID Setup", "", False)
	if cuid = "" then  ' This prevents the script from saving an empty string as the input if the field is left blank (even when canceling)
		Exit Function
	End If
	Database.Item("cuid") = cuid				'New input is passed to dictionary
	Call SaveDB(Database, AutoTIDConfig)		'New input is saved to AutoTIDConfig
	crt.Dialog.MessageBox("CUID updated.")
End Function

Function ClearDB(db_name)   	' Removes all Dictionary entries.
	'http://wiki.uniformserver.com/index.php/CGI:_VBScript_dictionary_file
	db_name.RemoveAll
End Function

function createwindow()
	rem source http://forum.script-coding.com/viewtopic.php?pid=75356#p75356
	dim signature, shellwnd, proc
	on error resume next
	signature = left(createobject("Scriptlet.TypeLib").guid, 38)
	do
		set proc = createobject("WScript.Shell").exec("mshta ""about:<head><script>moveTo(-32000,-32000);</script><hta:application id=app border=dialog minimizebutton=no maximizebutton=no scroll=no showintaskbar=yes contextmenu=no selection=yes innerborder=no icon=""%windir%\system32\notepad.exe""/><object id='shellwindow' classid='clsid:8856F961-340A-11D0-A96B-00C04FD705A2'><param name=RegisterAsBrowser value=1></object><script>shellwindow.putproperty('" & signature & "',document.parentWindow);</script></head>""")
		do
			if proc.status > 0 then exit do
			for each shellwnd in createobject("Shell.Application").windows
				set createwindow = shellwnd.getproperty(signature)
				if err.number = 0 then exit function
				err.clear
			next
		loop
	loop
end function
	sub hta_onunload
	completed = 1
	end sub

	sub hta_cancel
	completed = 2
	end sub

	sub hta_ok
	completed = 3
	end sub


Function ExitDevice(tab, strBase) ' Sends "exit" to exit out of devices. Contains checks for Red ADVAs and Blue switches.
	If Testing = 1 then
		Call TabName("Exit", tab)
	End If
	tab.Screen.send VbCr
	tab.Screen.send "exit" & VbCr
	
	If AccedianDevice.Test(strBase) then ' If user entered TID is for a NID.
		CheckForError = tab.Screen.ReadString(":",1)  ' Accedians use exit, ADVAs use logout
													  ' But both devices have the same hostname format
													  ' on Red...
		If inStr(CheckForError, "Error") > 0 then ' If error, adva detected, logout.
			tab.Screen.send "logout" & VbCr ' Absolutely has to be a lower case "l".
		End If
	End If

	If swDevice.Test(strBase) then ' Sends an extra "exit" when in blue switches. Blue switch specific quirk.
								   ' The issue here is that the first exit logs the user out of the enable mode. 
								   ' The second exit logs the user out of the device completely.
		tab.Screen.send "exit" & VbCr
	End If
End Function

Function ExitorLogout() ' Checks the currently logged into device, and determines if an "exit" or "logout" is required.
	strBase = crt.Screen.Get(crt.Screen.CurrentRow, 1, crt.Screen.CurrentRow, crt.Screen.CurrentColumn) 'Grabs the latest line in SecureCRT
		
	If LogoutDevices.Test(strBase) then ' If string matches a regex for the Logout-able devices.
		exitMethod = "logout"

	ElseIf ExitDevices.Test(strBase) then ' If string matches a regex for the exit-able devices.
		exitMethod = "exit"	
	End If
	
	If exitMethod = "exit" then 'Determined device requires "Exit" to leave
		Call ExitDevice(currentTab, strBase)
	ElseIf exitMethod = "logout" then 'Determined device require "Logout" to leave
		Logout(currentTab)
	ElseIf exitMethod = "Red Base" or exitMethod = "Bastion" then 'Determines if the User is in the base Red or Orange ecosystems
		' Continue. Leaving this here incase of future need.
	End If
	crt.Sleep 100 ' Only really comes into play when using the Ring Validator Tool.
End Function

Sub Del(obj)
	' Deletes of dictionaries, arrays, and variables
	If IsObject(obj) Then
		If LCase(TypeName(obj)) = "dictionary" Then
			obj.RemoveAll ' Remove all key, item pairs
		End If
		Set Obj = Nothing ' Disassociate
	ElseIf IsArray(obj) Then
		Erase obj ' Clear the array
	End If
	obj = Empty ' Uninitialize
End Sub

Function destinationTIDType(strTID)
	If RedDevices.Test(strTID) then
		destinationTIDType = "Red"

	ElseIf OrangeDevices.Test(strTID) then	'Tid is an Orange device (non er/es)
		If erDevice.Test(strTID) then
			strTID = Replace(strTID, ".", "-")
		End If
		destinationTIDType = "Orange"
		
	ElseIf redBastion.Test(strTID) then ' User typed in "Red" and wants to go to the red bastion.
		destinationTIDType = "Nocsup"
		
	ElseIf cnDevice.Test(strTID) then
		destinationTIDType = "CentralNID"
		
	ElseIf redNIDDevice.Test(strTID) then
		destinationTIDType = "RedNID"
		
	ElseIf orangeBastion.Test(strTID) or _
			circuitIDRegex.Test(strTID) or _
			OrangeCLLIRegex.Test(strTID) or _
			MetroRingID.Test(strTID) or _
			RouteTarget.Test(strTID) or _
			inStr(UCase(strTID), ("CONFIG")) > 0 then ' User typed in "orange" and wants to go to the orange bastion.
		destinationTIDType = "Bastion"

	Else	'Tid is not supported
		'msgbox(strTID)
		Call UnsupportedTidError(strTID)
		destinationTIDType = "Null"
		Exit Function
	End If
End Function

Function GetCurrentBastion(tab)
	timeout = 0
	Do
		
		If Testing = 1 then
			Call TabName("GetCurrentBastion = " & timeout, tab)
		End If
		tab.Screen.Synchronous = False
		crt.Sleep(500)		' Loop runs every half second
		tab.Screen.Synchronous = True
		timeout = timeout + 1
	
		result = crt.Screen.Get(crt.Screen.CurrentRow, 1, crt.Screen.CurrentRow, crt.Screen.CurrentColumn) 'Grabs the latest line in SecureCRT
		
		If timeout = 2 then
			tab.Screen.Send VbCr
		End If
	
		If inStr(result, "u@h") > 0 or inStr(result,"nocsup") > 0 then ' Determined the User is currently in a Red bastion server.
			GetCurrentBastion = "Red"
			Exit Do
			
		ElseIf inStr(result, "geofront") > 0 or inStr(result, "citadel") > 0 or inStr(result, "madgreens") > 0 or inStr(result, "00$") > 0 then
			' Determined that the User is currently in an Orange bastion server.
			GetCurrentBastion = "Orange"
			Exit Do
			
		ElseIf timeout = 40 then
			UnableToDetermineBastionError(result)
			Exit Do
		End If
	Loop
End Function

Function GetMyDocumentsFolder()		' Locate and save file path to MyDocuments.
	'Used in conjuction with Setup() to help create and place the AutoTIDConfig File in Windows
    Dim myShell
    Set myShell = CreateObject("WScript.Shell")
    GetMyDocumentsFolder = myShell.SpecialFolders("MyDocuments")
End Function

Function GetOrangeUsername(orangeUsername):
	If Database("orangeUsername") = "" then
		Database("orangeUsername") = crt.Dialog.Prompt("Username", "Missing entry in database file", "", False)
		Database.Item("orangeUsername") = Database("orangeUsername")
		Call SaveDB(Database, AutoTIDConfig)	' Saves the newly learned username in the AutoTIDConfig file.
	Else
		GetOrangeUsername = Database("orangeUsername")
	End If
End Function

Function Help()
	Call msgbox("Version: " + version & _
					VbCr & _
					VbCr & _
					"STORAGE =====================================" & _
					VbCr & _
					VbCr & _
					"Global settings and passwords are stored in " & AutoTIDConfig & _
					VbCr & _
					"File specific settings are at the top of the script." & _
					VbCr & _
					VbCr & _
					"COMMANDS ==================================" & _
					VbCr & _
					VbCr & _
					"Type ""password"" to change your password." & _
					VbCr & _
					"Type ""config"" to open a new tab to build an ADVA flow." & _
					VbCr & _
					"Type ""setup"" to rerun the initial setup." & _
					VbCr & _
					"Type ""geofront"", ""citadel"", or ""madgreens"" to change your default bastion server." & _
					" This has no effect on using the ""config"" command." & _
					VbCr & _
					"Type ""ring val"" to begin the ring validation process." & _
					VbCr & _
					"Type ""ring val man"" to paste your own TIDs for the Ring Val subprogram." & _
					VbCr & _
					"Paste your Circuit ID, CLLI, or METRO ID to run a search.pl command in geofront." & _
					VbCr & _
					VbCr & _
					"CONTACT US ==================================" & _
					VbCr & _
					"Please email incoming+Tagmeh/AutoTID@incoming.gitlab.com with criticism or visit https://gitlab.com/Tagmeh/AutoTID for more detail and to grab the latest version.", 0, "AutoTID Help")
End Function

Function inputboxml(prompt, title, defval)
	dim window
	set window = createwindow()
	completed = 0
	defval = replace(replace(replace(defval, "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
	with window
		with .document
			.title = title
			.body.style.background = "buttonface"
			.body.style.fontfamily = "consolas, courier new"
			.body.style.fontsize = "8pt"
			.body.innerhtml = "<div><center><nobr>" & prompt & "</nobr><br><br></center><textarea id='hta_textarea' style='font-family: consolas, courier new; width: 100%; height: 580px;'>" & defval & "</textarea><br><button id='hta_cancel' style='font-family: consolas, courier new; width: 85px; margin: 10px; padding: 3px; float: right;'>Cancel</button><button id='hta_ok' style='font-family: consolas, courier new; width: 85px; margin: 10px; padding: 3px; float: right;'>OK</button></div>"
		end with
		.resizeto 700, 700
		.moveto 550, 270
	end with
	window.hta_textarea.focus
	set window.hta_cancel.onclick = getref("hta_cancel")
	set window.hta_ok.onclick = getref("hta_ok")
	set window.document.body.onunload = getref("hta_onunload")
	do until completed > 0
		crt.sleep 10
	loop
	select case completed
	case 1
		inputboxml = ""
	case 2
		inputboxml = ""
		window.close
	case 3
		inputboxml = window.hta_textarea.value
		window.close
	end select
end function

Function Logout(tab) ' Sends "logout" to log out of devices.
	If Testing = 1 then
		Call TabName("Logout", tab)
	End If
	tab.Screen.send VbCr
	tab.Screen.send "logout" & VbCr
End Function

Function LoadDB(db_name,file_name)		' Load Dictionary values from AutoTIDConfig file.
	'http://wiki.uniformserver.com/index.php/CGI:_VBScript_dictionary_file
	Dim objFSO,objFile,line,pair_array,decoded_index,decoded_item
	Const ForReading = 1, ForWriting = 2, ForAppending = 8, ReadOnly = 1

	Set objFSO = CreateObject("Scripting.FileSystemObject") 	'Create file object
	Set objFile = objFSO.OpenTextFile(file_name, ForReading)	'Open for read

	Do Until objFile.AtEndOfStream            					'Read to end of file
		line = objFile.ReadLine                 				'Read line from file
		pair_array = split(line," = ")           				'Split Index-Item pair
		decoded_index = pair_array(0) 							'Decode Index - Removed encoding
		decoded_item  = pair_array(1) 							'Decode Item - Removed encoding
		db_name.Add decoded_index,decoded_item  				'Add to Dictionary
	Loop                                      					'Read another line
	objFile.Close
End Function

Function NidTypeCheck(tab, strTID)
	If Testing = 1 then
		Call TabName("NidTypeCheck", tab)
	End If
	AccedianQuestion = MsgBox("Is this an Accedian?", vbYesNo)
	If AccedianQuestion = 6	then 'Is an Accedian
		AccedianColor = MsgBox("Is this a Red Accedian?", vbYesNo)
		
			If AccedianColor = 6 then ' Red Accedian
				ConfiguredAccedian = MsgBox("Is the Accedian configured?", vbYesNo)
				tab.Screen.Send "ssh admin@" & Trim(strTID) & VbCr
				
				If ConfiguredAccedian = 6 then	'Is configured
					Call PasswordLoop(tab, Database("redAccedian"), strTID)
				ElseIf ConfiguredAccedian = 7 then	' Is not configured
					Call PasswordLoop(tab, "admin", strTID)
				End If
				
			ElseIf AccedianColor = 7 then ' Blue Accedian
				msgBox("This is a low priority feature. Hit up John Epperson if this is something you run into a lot.")
				' BlueNid = strTID
				' check = tab.Screen.get2(row, column+300, row-2, column-300)  ' Grabs the current line, and the full line above it.
				
				' if inStr(UCase(check), UCase(strTID)) > 0 then ' this doesn't actually run atm. upstream code logs out of the device before checking for the NID =/
					' tab.Screen.Send "ssh router 900 admin@" & strTID & VbCr
					' Call PasswordLoop(tab, "admin", strTID)
				' Else
					' PromptUserForDestinationTID("Blue Accedian")
				' End If
				' Call RedOrangetoRedOrange(tab, strTID)
				' tab.Screen.Send "ssh router 900 admin@" & BlueNid & VbCr
				' tab.Screen.Waitforstring "password"
				' Call PasswordLoop(tab, "admin", strTID)
				' strTID = BlueNID
			End If	
			strTID = "ACC @ " & strTID
			Call TabName(strTID, tab)

	ElseIf AccedianQuestion = 7 then	'Is an ADVA
		ConfiguredADVA = MsgBox("Is the Red ADVA configured?", vbYesNo)

		If ConfiguredADVA = 6 then	'Is configured
			tab.Screen.Send "ssh " & Trim(strTID) & VbCr
			password = crt.Dialog.Prompt("One Time Password ", "OTP Prompt", "", False)
			Call PasswordLoop(tab, password, strTID)
		ElseIf ConfiguredADVA = 7 then	'Is not configured
			tab.Screen.Send "ssh root@" & Trim(strTID) & VbCr
			Call PasswordLoop(tab, Database("redADVA"), strTID)
		End If
		strTID = "ADVA @ " & strTID
		Call TabName(strTID, tab)
	End If
End Function

Function NocsupRociLoop(tab) ' Designed to pause the script while logging out of the Orange bastion.
	If Testing = 1 then
		Call TabName("NocsupRociLoop", tab)
	End If
	Do
		tab.Screen.Synchronous = False
		crt.Sleep(500)		' Loop runs every half second
		tab.Screen.Synchronous = True
		row = tab.Screen.CurrentRow
		column = tab.Screen.CurrentColumn
		check = tab.Screen.get2(row, column+300, row-3, column-300)  ' Grabs the current line, and the full line above it.
		If inStr(check, "@nocsup") > 0 then
			Exit Do
		End If
	Loop
End Function

Function PasswordLoop(tab, password, strTID)
	timeout = 0
	'msgbox("PasswordLoop")
	Do
		If Testing = 1 then
			Call TabName("PasswordLoop = " & timeout, tab)
		End If
		tab.Screen.Synchronous = False
		crt.Sleep(750)		' Loop runs every 3/4 second
		tab.Screen.Synchronous = True
		timeout = timeout + 1
		row = tab.Screen.CurrentRow
		column = tab.Screen.CurrentColumn
		check = tab.Screen.get2(row, column+300, row-2, column-300)  ' Grabs the current line, and the full line above it.
		'msgbox(check + " " + password)
		If 	inStr(check, "^C") > 0 or _
			inStr(check, "port 22: No route to host") > 0 or _
			inStr(check, "telnet: could not resolve") > 0 or _
			inStr(check, "ssh: Could not resolve hostname") > 0 or _
			inStr(check, "Connection Closed by remote hos") > 0 or _
			inStr(check, "telnet: Unable to connect to remote host: Connection refused") > 0 then
			If Database("RingValisRunning") = 1 then
				Database("SkipRingNode") = 1
				Exit Function
			End If
			' If canceled by the user, stops the script
			tab.Screen.Send "# AutoTID Canceled" & VbCr
			Exit Do
		
		ElseIf inStr(check, strTID & ">") then
			Exit Do
			'Graceful Exit, Pretty sure this is exclusively for the GES/MES devices.
		
		ElseIf timeout >= 30 and Database("RingValisRunning") = 1 then
			tab.Screen.Send chr(3) & VbCr
			Database("SkipRingNode") = 1
			Exit Do
			
		ElseIf passwordRegex.Test(Trim(check)) then  ' Looking for "Password" or "password".
			tab.Screen.Send password & VbCr
			tab.Screen.Send VbCr
			'msgbox("passwordRegex Sent" + password)
			Exit Do

		ElseIf inStr(check, "yes/no)?") > 0 then	' Triggered if this is the first time logging into a box.
			tab.Screen.Send "Yes" & VbCr
			tab.Screen.Send VbCr
			Call PasswordLoop(tab, password, strTID) 	' Reruns this function in anticipation of the password field.
			Exit Do

		ElseIf inStr(check, "port 22: Connection refused") > 0 or _
				inStr(check, "ssh_exchange_identification: Connection closed by remote host") > 0 then		' If the box cannot be logged into by SSH
			tab.Screen.send "telnet " & strTID & VbCr
			telnetLogin = tab.Screen.WaitForStrings("sername", "ogin", 4)
			If Database("RingValisRunning") = 1 and telnetLogin = 0 then
				Database("SkipRingNode") = 1
				Exit Function
			End If
		
		ElseIf inStr(check, "sername") > 0 or inStr(check, "Login") > 0 then
			tab.Screen.send Database("cuid") & VbCr	' Sends the username, and doesn't exit the loop in anticipation of the password prompt.

		ElseIF inStr(check, "nocsup") > 0 then
			check = tab.Screen.get2(row, column+300, row-3, column-300)

			' Watches for a ROCI error, and then attempts to SSH into the device
			If inStr(check, "Access Denied - Unable to determine vendor/type") > 0 then
				tab.Screen.send VbCr
				crt.Sleep 500
				tab.Screen.send "ssh " & strTID & VbCr
				password = crt.Dialog.Prompt("One Time Password ", "OTP Prompt", "", False)
				tab.Screen.Send password & VbCr

			' Runs if SSH fails with one of the below error messages. Attempts to telnet using the username and OTP.
			ElseIf  inStr(check, "Authentication failed.") > 0 or _
					inStr(check, "ssh: Could not resolve hostname") > 0 or _
					inStr(check, "Connection closed by remote host") > 0 or _
					inStr(check, "ssh_exchange_identification: Connection closed by remote host") > 0 then
				tab.Screen.send VbCr
				tab.Screen.send "telnet " & strTID & VbCr
				
			ElseIf inStr(check, "login :") > 0 then
				tab.Screen.send Database("cuid") & VbCr	' Sends the username, and doesn't exit the loop in anticipation of the password prompt.
				password = crt.Dialog.Prompt("One Time Password ", "OTP Prompt", "", False)
				tab.Screen.Send password & VbCr
				Exit Do
			
	
			' Cases by which the loop ends.
			ElseIf  inStr(check, "^C") > 0 or _
					inStr(check, "level3.com") > 0 or _
					inStr(check, "hooting steps.") > 0 or _
					inStr(check, "Error: Bad command") > 0 or _
					inStr(check, "port 22: No route to host") > 0 or _
					inStr(check, "telnet: could not resolve") > 0 or _
					inStr(check, "Connection closed by foreign host.") > 0 or _
					inStr(check, "Also see http://matrix-tool.idc1.level3.com/wiki/index.php/ROCi_Introduction") > 0 then
					tab.Screen.Send "# AutoTID Canceled" & VbCr
				Exit Do

			ElseIf  inStr(check, strTID + ">") or _
					inStr(check, LCase(strTID) + "#") or _
					inStr(check, UCase(strTID) + "#") then
					'Graceful Exit. No need to tell the User the Script ended.
				Exit Do
				
			End If
		ElseIf timeout >= 40 then
				tab.Screen.Send "#AutoTID timed out after 20 seconds." & VbCr
				Exit Do

		End If
	Loop
	'msgBox("PasswordLoop ended")
End Function

Function PromptUserForDestinationTID(PromptType) ' Prompts user for input based on the PromptType given.
	If inStr(PromptType, "AutoTID") > 0 then
		strTID = crt.Dialog.Prompt("Enter a TID below, or type ""help"".", "Navigation prompt Version 2", "", False) ' User input for the AutoTID script.
		
	ElseIf inStr(PromptType, "RingVal") > 0 then
		strTID = crt.Dialog.Prompt("Enter the drop switch below.", "Ring Validator", "", False)	' User input for the Ring Val Subprogram.
		
	ElseIf inStr(PromptType, "Blue Accedian") > 0 then
		strTID = crt.Dialog.Prompt("ESP: ", "Blue Accedian ESP Prompt", "", False) ' User input for when logging into a blue NID.
		
	End If
	
	
	strTID = Replace(Replace(Replace(Replace(Replace(Replace(Replace(strTID, Chr(12), ""), Chr(16), ""), Chr(10), ""), Chr(11), ""), Chr(13), ""), vbTab, "")," ","",1,-1)	' Like a death trap for any whitespace monster. Might be able to simplify this with regexes.
	strTID = ThingstoStripfromUserInput.Replace(strTID, "") ' Cleans the ASRI shit off of the input.
	PromptUserForDestinationTID = strTID
End Function 


Function RedOrangetoRedOrange(tab, strTID, currentBastion, destinationTID)
	'Used to determine if User is in Red or Orange ecosystem, then determines where User is trying to go based on strings in their respective arrays.
	If Testing = 1 then
		Call TabName("RedOrangetoRedOrange", tab)
	End If
	
	If destinationTID = "Null" then ' Throws an custom error for the user
		Exit Function ' Then exits the script
	End If
	
	tab.Screen.Send VbCr
	
	If currentBastion = "Red" then
		If destinationTID = "Red" then ' Red ecosystem to Red device.
			tab.Screen.Send "roci " & strTID & VbCr
			Call PasswordLoop(tab, "", strTID)
			
		Elseif destinationTID = "Orange" then ' Red ecosystem to an Orange device.
			RedtoOrange(tab) ' Sends the correct orange bastion command
			Call PasswordLoop(tab, Database("userPass"), strTID) ' Sends orange password and monitors for errors
			tab.Screen.WaitForString "$" ' Wait till completely loggged into the orange bastion
			tab.Screen.Send "ssh " & Database("cuid") & "@" & strTID & VbCr
			Call PasswordLoop(tab, Database("cuidPass"), strTID) ' Sends orange password and monitors for errors
			
		Elseif destinationTID = "CentralNID" then
			' NocsupRociLoop(tab)
			tab.Screen.Send "ssh admin@" & strTID & VbCr
			Call PasswordLoop(tab, Database("redAccedian"), strTID)
		
		Elseif destinationTID = "RedNID" then
			' NocsupRociLoop(tab)
			Call NidTypeCheck(tab, strTID)

			
		Elseif destinationTID = "Nocsup" then ' Red ecosystem to Red Bastion. Just logs out of w/e Red device it's currently in.
			Call ExitorLogout()
			' NocsupRociLoop(tab)
			
		Elseif destinationTID = "Bastion" then ' Red ecosystem to Orange Bastion.
			Call ExitorLogout()
			Call RedtoOrange(tab)
			Call PasswordLoop(tab, Database("userPass"), strTID)
			tab.Screen.WaitForString "$"

		End If	
	
	Elseif currentBastion = "Orange" then
		'msgBox("current bastion is orange")
		If destinationTID = "Red" then ' Orange ecosystem to a Red device.
			Logout(tab)
			NocsupRociLoop(tab)
			tab.Screen.Send "roci " & strTID & VbCr
			Call PasswordLoop(tab, "", strTID)
			
		Elseif destinationTID = "Orange" then ' Orange ecosystem to an Orange device.
			Call ExitorLogout()
			tab.Screen.Send "ssh " & Database("cuid") & "@" & strTID & VbCr
			Call PasswordLoop(tab, Database("userPass"), strTID)
			
		Elseif destinationTID = "CentralNID" then
			Logout(tab)
			NocsupRociLoop(tab)
			tab.Screen.Send "ssh admin@" & strTID & VbCr
			Call PasswordLoop(tab, Database("redAccedian"), strTID)
		
		Elseif destinationTID = "RedNID" then
			Logout(tab)
			NocsupRociLoop(tab)
			Call NidTypeCheck(tab, strTID)		
			
		Elseif destinationTID = "Nocsup" then ' Orange to Red Bastion.
			Call ExitorLogout()
			Logout(tab)
			
		Elseif destinationTID = "Bastion" then ' Orange to Orange bastion.
			Call ExitorLogout()
		Else
		End If	
	End If
End Function

Function RedtoOrange(tab)		'Connecting to the Orange network from the Red network
	If Testing = 1 then
		Call TabName("RedtoOrange", tab)
	End If
	tab.Screen.send VbCr
	Select Case Database("bastionServer")
	Case ""	' Default
		tab.Screen.Send "ssh geofront.ctl.one" & VbCr
	Case 1
		tab.Screen.Send "ssh geofront.ctl.one" & VbCr
	Case 2
		tab.Screen.Send "ssh citadel.ctl.one" & VbCr
	Case 3
		tab.Screen.Send "ssh madgreens.ctl.one" & VbCr
	End Select
End Function

Function ReloadDB(db_name,file_name)	' Deletes Dictionary entries and recreates Dictionary.
	'http://wiki.uniformserver.com/index.php/CGI:_VBScript_dictionary_file
	db_name.RemoveAll                   		'Delete dictionary content
	Call LoadDB(db_name,file_name)  			'Load DB from file
End Function

Function RingVal(buildRing, ManualRingListInput)
	Set objDict = CreateObject("Scripting.Dictionary")
	Set objInterfaceDict = CreateObject("Scripting.Dictionary")
	Set vlanDict = CreateObject("Scripting.Dictionary")
	Set interfacesDict = CreateObject("Scripting.Dictionary")
	
	Database("RingValisRunning") = 1
		
	If ManualRingListInput = 1 then		
		ringList = inputboxml("Enter Ring Nodes:", "Multiline inputbox via HTA", "")
	
		If ringList = "" then ' Stops the script if the user closes the notepad window without any text.
			Exit Function
		End If
		
	Else
		strTID = PromptUserForDestinationTID("RingVal")
		If strTID = "" then
			Exit Function
		End If	
	End If
	
	vlan = crt.Dialog.Prompt("VLAN:", "VLAN Box", "", False)	'Takes the User's input

	If	vlan = "" then				'Close script if there's no user entered data
		Exit Function	

	Elseif vlan < 1 or vlan > 4094 then
		Call msgBox("Vlan must be between 1 and 4094", 48, "Bad VLAN range")
		vlan = crt.Dialog.Prompt("VLAN:", "VLAN Box", "", False)	'Takes the User's input
		vlan = Trim(Replace(vlan, vbTab, ""))				'Removes blank spaces on either side of the User's input and Removes tabs before and after TID 

	End If
	
	Set tab = crt.GetScriptTab				'grab the current tab (where the script started running)
	If OpenRingValinNewTab = 1 then
		Set tab = tab.Clone			 		'clone the current tab	
	Else
		tab.Screen.Send VbCr
		ExitorLogout()
	End If
		
	If ManualRingListInput = 0 then
		Call RedOrangetoRedOrange(tab, strTID, GetCurrentBastion(tab), destinationTIDType(strTID))
		stopTID = strTID + "#"
		tab.Screen.WaitForString strTID,1
		tab.Screen.Send VbCr
		tab.Screen.Send "show rep topology" & VbCr
		tab.Screen.Send chr(32)
		tab.Screen.Send chr(32)
		tab.Screen.Send chr(32)
		tab.Screen.Send chr(32)
		tab.Screen.Send "##End##" & VbCr

		ringList = tab.Screen.ReadString("##End##", 2)
		ringList = Split(ringList, "---------------- ---------- ---- ----")(1)
	End If
		
	Call TabName("Gathering Data", tab)	

	Set myMatches = OrangeRingDevice.Execute(ringList)
	For Each Match in myMatches
		If Not objDict.Exists(Trim(CStr(Match.Value))) then objDict.Add Trim(CStr(Match.Value)), 1
		Next


	TheList = objDict.Keys
	builtInterfaces = 0
	current = 0				' current number of device
	max = UBound(TheList) + 1	' max number of devices
	
	tab.Screen.Send VbCr
	tab.Screen.Send VbCr

	For Each strTID in TheList
		On Error Resume Next
		Do	' null loop for logic short-circuit
			current = current + 1
			crt.Sleep(500)

			row = tab.Screen.CurrentRow
			column = tab.Screen.CurrentColumn
			strBase = tab.Screen.Get(row, 1, row, column)

			strTIDBreak = strTID + "#"
			' Call msgBox(strTID + vbcr + strTIDBreak)

			ExitorLogout()
			
			Call RedOrangetoRedOrange(tab, strTID, GetCurrentBastion(tab), destinationTIDType(strTID))
			
			
			If Database("SkipRingNode") = 1 then
				vlanDict.Add strTID, "Login Request Failed"
				Database("SkipRingNode") = 0
				Exit Do
			Else
				tab.Screen.WaitForString "#"
			End If
			
			If buildRing = 1 then				
				Call TabName("Building " & current & "/" & max, tab)
				tab.Screen.Send "config t" & VbCr
				tab.Screen.Send "vlan " & vlan & VbCr
				tab.Screen.WaitForString ")#"
				tab.Screen.Send "end" & VbCr
				tab.Screen.WaitForString "#"
				tab.Screen.Send "wr" & VbCr
				tab.Screen.WaitForString "#"
			Else 
				Call TabName("Validating " & current & "/" & max & " || " & "Built: " & builtInterfaces & "/" & current-1, tab)
				
			End If
			
			' runs if the device is a ZG or ZF
			If asr920Device.Test(strTID) then
				tab.Screen.Send "show bridge-domain " & vlan & VbCr
				tab.Screen.Send "#END#"
				asr920Output = tab.Screen.ReadString("#END#")
				asr920Output = Split(asr920Output, strTIDBreak + "show bridge-domain " + vlan)(1)
				vlanLine = Split(asr920Output, vbcrlf)(1)		' Bridge-domain 6 (2 ports in all)
				line2 = Split(asr920Output, vbcrlf)(2) 			' State: UP                Mac Learning: Enabled
				
				Set Interfaces = OrangeInterfaces.Execute(asr920Output)
				For Each Iface in Interfaces
					If Not objInterfaceDict.Exists(Trim(CStr(Iface.Value))) then objInterfaceDict.Add Trim(CStr(Iface.Value)), 1
					Next
					
				InterfaceList = objInterfaceDict.Keys	
				
				InterfacesMax = UBound(InterfaceList) + 1
				CurrentInterface = 1
				For Each Iface in InterfaceList

					If inStr(Iface, "Ten") > 0 then
						interfacesOutput = Replace(Iface, "nGigabitEthernet", "") 
					Elseif inStr(Iface, "Gig") > 0 then
						interfacesOutput = Replace(Iface, "gabitEthernet", "") 
					Elseif inStr(Iface, "Fast") > 0 then
						interfacesOutput = Replace(Iface, "stEthernet", "") 
					End If
					
					If CurrentInterface = InterfacesMax then
						interfacesOutputFinal = interfacesOutputFinal + interfacesOutput
					Else
						interfacesOutputFinal = interfacesOutputFinal + interfacesOutput & ", "
						CurrentInterface = CurrentInterface +1
					End If
				Next

				If inStr(vlanLine, vlan) > 0 then
					vlanDict.Add strTID, "Yes"
					interfacesDict.Add strTID, interfacesOutputFinal
					interfacesOutputFinal = ""
					builtInterfaces = builtInterfaces +1

				Else
					'msgBox(vlan & " is not in " & vlanLine)
					vlanDict.Add strTID, "No "
					interfacesDict.Add StrTID, "---"
					interfacesOutputFinal = ""


				End If
				objInterfaceDict.RemoveAll()
				

			' Runs on W2, C6, C4, etc devices.
			Else
				tab.Screen.Send "show vlan id " & vlan & VbCr
				tab.Screen.Send chr(3) & VbCr
				tab.Screen.Send "#END#"
				vlanOutput = tab.Screen.ReadString("#END#")
				interfacesOutput = Split(vlanOutput, "active")(1)
				interfacesOutput = Trim(Split(interfacesOutput, vbcrlf)(0))
				tab.Screen.Send VbCr
				tab.Screen.WaitForString strTID
				tab.Screen.Send "show mac address-table dynamic vlan " & vlan & VbCr
				
				If inStr(vlanOutput, "not found in current VLAN database") > 0 then
					vlanDict.Add strTID, "No "
					interfacesDict.Add strTID, "---"
				else
					vlanDict.Add strTID, "Yes"
					interfacesDict.Add strTID, interfacesOutput
					builtInterfaces = builtInterfaces +1

				End If
			End If
			tab.Screen.Send VbCr
			tab.Screen.WaitForString strTID
			Exit Do
		Loop While False
	Next
	Call TabName("Building Output", tab)
	' tab.Screen.Send "exit" & VbCr

	' tab.Screen.WaitForString "$"
	ExitorLogout()
	Call RedOrangetoRedOrange(tab, "Orange", GetCurrentBastion(tab), destinationTIDType("Orange"))
	
	tab.Screen.Send  VbCr

	tab.Screen.Send "'" & VbCr
	tab.Screen.Send VbCr
	tab.Screen.Send VbCr
	tab.Screen.Send "#          Validating Ring for VLAN " & vlan & VbCr
	tab.Screen.Send "#----------------------------------------------------" & VbCr
	tab.Screen.Send "#    TID           VLAN Built    Interfaces" & VbCr
	tab.Screen.Send "#----------------------------------------------------" & VbCr

	For Each strTID in TheList
		'On Error Resume Next
		crt.Sleep(50)
		tab.Screen.Send "#" & strTID & "        " & vlanDict(strTID) & "        " & interfacesDict(strTID) & VbCr
	Next
	crt.Sleep 100
	tab.Screen.Send chr(3) & VbCr
	Call TabName("Validation Complete", tab)
End Function

Function SaveDB(db_name,file_name)		' Saves Dictionary vales to AutoTIDConfig file.
	'http://wiki.uniformserver.com/index.php/CGI:_VBScript_dictionary_file
	'Takes Dictionary name and AutoTIDConfig
	Dim objFSO,objFile,line,key_array,strKey
	Const ForReading = 1, ForWriting = 2, ForAppending = 8, ReadOnly = 1

	Set objFSO  = CreateObject("Scripting.FileSystemObject")      	'Create object
	Set objFile = objFSO.OpenTextFile(file_name, ForWriting, True)	'Open for write

	key_array = db_name.Keys                                      	'Get all Keys
	For Each strKey in key_array                                  	'Scan array
		line = strKey & " = " & db_name.Item(strKey)  				'Encode - Removed encoding
		objFile.WriteLine(line)                                    	'Write to file
	Next
	objFile.Close
End Function

Function Searchpl(strTID, command, NewTab)
	Set tab = crt.GetScriptTab
	If NewTab = 1 then
		Set tab = tab.Clone
		Bastion = 1
		RedtoOrange(tab)
		Call PasswordLoop(tab, Database("userPass"), strTID)
	Else
		ExitorLogout()
		Call RedOrangetoRedOrange(tab, strTID, GetCurrentBastion(tab), destinationTIDType(strTID))
	End If
	
	timeout = 0
	Do
		tab.Screen.Synchronous = False
		crt.Sleep(500)		' Loop runs every half second
		tab.Screen.Synchronous = True
		timeout = timeout + 1
	
		result = crt.Screen.Get(crt.Screen.CurrentRow, 1, crt.Screen.CurrentRow, crt.Screen.CurrentColumn) 'Grabs the latest line in SecureCRT
		If inStr(result, "$") > 0 then
			Exit Do
		End If
	Loop

	Call TabName("search.pl " & command, tab)
	tab.Screen.Send VbCr
	tab.Screen.Send "search.pl " & command & strTID & VbCr
End Function

Function SelectSubprogram(strTID)
	If inStr(UCase(strTID), ("CUID")) > 0 AND inStr(UCase(strTID), ("PASS")) > 0 then
		'User wants to change their CUID password
		ChangeCuidPassword()
		SelectSubprogram = "Null"
		Exit Function
		
	ElseIf inStr(UCase(strTID), ("PASSWORD")) > 0 then		'User wants to change their password
		ChangePassword()
		SelectSubprogram = "Null"
		Exit Function
		
	ElseIf inStr(UCase(strTID), ("CUID")) > 0 then		'User wants to change their CUID
		ChangeCuid()
		SelectSubprogram = "Null"
		Exit Function

	ElseIf inStr(UCase(strTID), ("GEOFRONT")) > 0 then 	'User wants to change their bastion server
		Call ChangeDefaultBastionServer("Geofront", 1)
		SelectSubprogram = "Null"
		Exit Function
		
	ElseIf inStr(UCase(strTID), ("CITADEL")) > 0 then
		Call ChangeDefaultBastionServer("Citadel", 2)
		SelectSubprogram = "Null"
		Exit Function
		
	ElseIf inStr(UCase(strTID), ("MADGREENS")) > 0 then
		Call ChangeDefaultBastionServer("Madgreens", 3)
		SelectSubprogram = "Null"
		Exit Function
		
	ElseIf inStr(UCase(strTID), ("VERSION")) > 0 then
		Set WshShell = CreateObject("WScript.Shell")
		strCurDir    = WshShell.CurrentDirectory
		Set WshShell = Nothing
		set xmlhttp = createobject ("msxml2.xmlhttp.3.0")
		xmlhttp.open "get", "https://gitlab.com/Tagmeh/AutoTID/raw/master/AutoTID.vbs", false
		xmlhttp.send
		strOutputFull = xmlhttp.responseText ' Full webpage
		strOutput = split(strOutputFull, "version = """)(1) ' remove things before version
		strOutput = split(strOutput, vbLf)(0) ' remove things after version number
		internetVersion = Trim(replace(strOutput, chr(34), "")) ' remove the last " 
		
		If version >= internetVersion then
			msgBox("You have the latest version" & _
			VbCr & _
			"Version: " + version)
		else
			msgBox("Version: " + internetVersion + " is ready to download" &_
			VbCr &_
			"Your version is: " + version &_
			VbCr &_
			VbCr &_
			"https://gitlab.com/Tagmeh/AutoTID/raw/master/AutoTID.vbs")
		end if
		
		SelectSubprogram = "Null"
		Exit Function
		
	ElseIf inStr(UCase(strTID), ("SETUP")) > 0 then
		If autoConfigFileObject.FileExists(AutoTIDConfig) then
			clearDB(Database)
			autoConfigFileObject.DeleteFile(AutoTIDConfig)
		End If
		Setup()
		SelectSubprogram = "Null"
		Exit Function

	ElseIf inStr(UCase(strTID), ("CONFIG")) > 0 then
		' Opens the ADVA Flow Config Perl script in a new Geofront tab.
		Call AdvaConfigGenerator(OpenAdvaConfigGeneratorinNewTab, strTID)
		SelectSubprogram = "Null"
		Exit Function

	ElseIf circuitIDRegex.Test(strTID) then
		'CircuitIDSearch()
		Call Searchpl(strTID, "i=", OpenCircuitIDSearchinNewTab)
		SelectSubprogram = "Null"
		Exit Function

	ElseIf OrangeCLLIRegex.Test(strTID) then
		'CLLISearch()
		Call Searchpl(strTID, "T=", OpenCLLISearchinNewTab)
		SelectSubprogram = "Null"
		Exit Function
		
	ElseIf RouteTarget.Test(strTID) then
		'Route Target search
		Call Searchpl(strTID, "-W=set | grep ", OpenRouteTargetinNewTab)
		SelectSubprogram = "Null"
		Exit Function
		
	ElseIf MetroRingID.Test(strTID) then
		' Logs into a Geofront session and uses the search.pl search tool to check the given metro ring identifier.
		Call Searchpl(strTID, "-T -N=", OpenMetroRingIDinNewTab)
		SelectSubprogram = "Null"
		Exit Function

	Elseif inStr(UCase(strTID), ("HELP")) > 0 then
		Help()
		SelectSubprogram = "Null"
		Exit Function
		
	Elseif inStr(UCase(strTID), ("RINGVAL")) > 0 then ' Ring Val Subprogram.
		If inStr(UCase(strTID), ("BUILD")) > 0 then ' Checks for, and enables the build option.
			buildRing = 1
		End If
		
		If inStr(UCase(strTID), ("MAN")) > 0 then ' Checks for, and enables the option to manually enter multiple TIDs.
			ManualRingListInput = 1
		End If
		
		Call RingVal(buildRing, ManualRingListInput)
		SelectSubprogram = "Null"
		Exit Function
		
	End If
End Function

Function SetMasterOpeninNewTab() ' Forces the other "inNewTab" options to 1 (yes) if the Master option is set to 1.
	If MasterOpeninNewTab = 1 then
		OpenCircuitIDSearchinNewTab = 1
		OpenCLLISearchinNewTab = 1
		OpenRingValinNewTab = 1
		OpenAutoTIDinNewTab = 1
		OpenAdvaConfigGeneratorinNewTab = 1
	ElseIf MasterOpeninNewTab = 2 then ' Special option if a logon script is found. Forces everything to open in the same tab.
		OpenCircuitIDSearchinNewTab = 0
		OpenCLLISearchinNewTab = 0
		OpenRingValinNewTab = 0
		OpenAutoTIDinNewTab = 0
		OpenAdvaConfigGeneratorinNewTab = 0
	End If
End Function

Function SetupPrompt()				' Runs if AutoTIDConfig doesn't exist.
	crt.Dialog.MessageBox("Welcome to AutoTID" & _
							VbCr & _
							VbCr & _
							"AutoTIDConfig.txt will be created to store your passwords." & _
							VbCr & _
							"Located in " & AutoTIDConfig & _
							VbCr & _
							VbCr & _
							" Type ""set password"" to change your password." & _
							VbCr & _
							VbCr & _
							" Moving the file to another directory will cause the script to run the initial setup again" & _
							VbCr & _
							VbCr & _
							" Right click on the file > properties > hidden, for added security" & _
							VbCr & _
							VbCr & _
							" Click OK to begin setup")
End Function


Function SetupWizard(AutoTIDConfig) ' Prompts the User for their username and password, then saves the data to a file.
	Database.Removeall ' Remove existing key/value pairs befor re-adding them below.
	
	orangeUsername = crt.Dialog.Prompt("Orange Username", "Orange Username Setup", "", False)
	
	userPass = crt.Dialog.Prompt("Orange Password", "Orange Password Setup", "", True)
	If userPass = "" then	'If user's password is left blank, end script
		Exit Function
	End If
	
	cuid = crt.Dialog.Prompt("User CUID", "User Setup", "", False)
	If cuid = "" then	'If user's cuid is left blank, end script
		Exit Function
	End If
	
	cuidPass = crt.Dialog.Prompt("User CUID Password", "User Setup", "", True)
	If cuid = "" then	'If user's cuid password is left blank, end script
		Exit Function
	End If
	
	accedianPassword = crt.Dialog.Prompt("Red Accedian Password", "Red Accedian Pw Setup", "Not ADMIN", False)
	If accedianPassword = "Not ADMIN" then	'False if accedianPassword is anything but "Not ADMIN"
		'Loop is designed to not allow the user to leave the default "Not ADMIN" password in the prompt
		Do While accedianPassword = "Not ADMIN"
			accedianPassword = crt.Dialog.Prompt("Red Accedian Password", "Red Accedian Pw Setup", "Not ADMIN", False)
		Loop
	End If
	
	advaPassword = "ChgMeNOW"  ' This is the default password that ADVA ships their devices out with. Not a secret.
	
	crt.Dialog.MessageBox("Geofront is the default bastion server" & _
							VbCr & _
							VbCr & _
							"Type ""Citadel"" or ""Madgreens"" at the AutoTID prompt to change it.")

	crt.Dialog.MessageBox("Setup is now complete")
	Set objFile = autoConfigFileObject.OpenTextFile(AutoTIDConfig, ForWriting, True)
	' Write out the results of the command to AutoTIDConfig in MyDocuments
	objFile.Close	' Close the log file

	'Adding varibles to dictionary
	Database.Add "orangeUsername", orangeUsername
	Database.Add "userPass", userPass
	Database.Add "redAccedian", accedianPassword
	Database.Add "redADVA", advaPassword
	Database.Add "bastionServer", "1"
	Database.Add "cuid", cuid
	Database.Add "cuidPass", cuidPass

	Call SaveDB(Database, AutoTIDConfig)		'Save dictionary values to AutoTIDconfig in MyDocuments
End Function

Function TabHasLogonScript() ' Checks for logon scripts, and if found, runs the script in the same window by default.
	session =  crt.Session.Path ' Grabs the current session (tab).
	Set objConfig = crt.OpenSessionConfiguration(session) ' Pulls the current session's config file.

	UseScriptFileOption = "Use Script File"
	UseLoginScriptOption = "Use Login Script"

	UseScriptFileOptionSet = objConfig.GetOption(UseScriptFileOption) ' Checks for the afore-specified config lines.
	UseLoginScriptOptionSet = objConfig.GetOption(UseLoginScriptOption) ' Checks for the afore-specified config lines.


	If UseScriptFileOptionSet = False and UseLoginScriptOptionSet = False then
		' No change, there are no logon scripts on this session
	Else
		MasterOpeninNewTab = 2' Logon script detected, not opening in a new window.
		' This solves the "There cannot be two scripts running at the same time" error SecureCRT will throw.
	End If
End Function

Function TabName(TID, tab)		'Used to rename the tab, is RenameTab is set to "1".
	If RenameTab = 1 then		'Located in "Settings"
		tab.caption = Trim(TID)	'Remove white spaces and rename current tab
	Else
		Exit Function
	End If
End Function

Function UnableToDetermineBastionError(result) ' Custom error that pops up when the script cannot determine where the user is currently.
	Call msgBox("Unable to determine bastion server." & _
	VbCr &_
	VbCr &_
	result & _
	VbCr &_
	VbCr &_
	"Error Code 0x31" & _
	VbCr &_
	"Version: " + version & _
	VbCr &_
	VbCr &_
	"Please send Jonathan Epperson an email at " & "incoming+Tagmeh/AutoTID@incoming.gitlab.com with above information.", 48, " Unknown TID")
End Function

Function UnsupportedTidError(strTID) ' Custom error that pops up when the user entered string is not recoginized.
	Call msgBox("""" & strTID & """" & " is an unknown or unsupported TID." & _
	VbCr &_
	VbCr &_
	"TID: " & strTID & _
	VbCr &_
	VbCr &_
	"Error Code 0x32" & _
	VbCr &_
	"Version: " + version & _
	VbCr &_
	VbCr &_
	"Please send Jonathan Epperson an email at " & _
	"incoming+Tagmeh/AutoTID@incoming.gitlab.com with above information.", 48, " Unknown/unsupported TID")
End Function


'------------------------------------------------------------
'----------------------------REGEX---------------------------
'------------------------------------------------------------

Set LogoutDevices = New RegExp ' These devices require "logout" to return to the bastion server.
With LogoutDevices
	.Pattern = "(([eaEA][sS][pPrR]\w\.[a-zA-Z]{3}\d)|(\w{8}4[pP]\d{3})|([eE][aA][rR]\d\.\w{3}\d)|((?!.*ges)^.*[a-zA-Z]{3}\d{2}\.[a-zA-Z]{3}\d+)|(\w{8}([eE][dD])\d{3}))"
	.IgnoreCase = True
	.Global = True
End With
' Can't force check the begining of the string with ^ because not all string snips start with the device name. ie: "*A:esp1.dal1#"

Set ExitDevices = New RegExp ' These devices require 'exit" to return to the bastion server.
With ExitDevices
	.Pattern = "((\w{8}\w\w\d{3})|([bB][aA][rR]\d\.\w{3}\d)|([phPH][rR]\d.\w{3}\d)|([gGmM][eE][sS]\d*\.\w{3}\d*)|([Nn][Ii][Dd]-.*\.\w{3}\d)|([Gg][Mm]\d*)|([vV][pP][eE]\d\.)|(cn\d{3}\.\w{3}\d)|((sw\d)|(sw\d\d))\.(\w{3}\d)|(\w{3}\d*[.-][eE]([rR]|[sS])\d*))"
	.IgnoreCase = True
	.Global = True
End With
' Forcing start of the string with ^ works in Orange and ges/mes devices

Set RedDevices = New RegExp
With RedDevices
	.Pattern = "^(([eaEA][sS][pPrR][0-9]{1,2}\.[a-zA-Z]{3}\d)|([bB][aA][rR]\d\.\w{3}\d)|([bB]?[eE][aA][rR]\d\.\w{3}\d)|([gGmM][eE][sS]\d*\.\w{3}\d*)|(^[phPH][rR]\d.\w{3}\d)|(((sw\d)|(sw\d\d))\.(\w{3}\d))|([vV][pP][eE]\d\.))"
	.IgnoreCase = True
	.Global = True
End With

' 10.(118|9[4-5]|254)\.([1-2][0-9][0-9]|[1-9][0-9]|[0-9])\.([1-2][0-9][0-9]|[1-9][0-9]|[0-9])
' (cn\d{3}\.\w{3}\d)

Set OrangeDevices = New RegExp
With OrangeDevices
	.Pattern = "(\w{3}\d*[.-][eE]([rR]|[sS])\d*)|(\w{8}\w\w\d{3})"
	.IgnoreCase = True
	.Global = True
End With

Set OrangeRingDevice = New RegExp
With OrangeRingDevice
	.Pattern = "(\w{8}(?!9[kKhH]|4[pP]|6[kKhH]|[nN]{2}|[rR][tT])\w\w\d{3})"
	.IgnoreCase = True
	.Global = True
End With

' Red Accedian NID and possibly Red ADVAs on the 10.118, 10.94, and 10.95 subnets.
Set redNIDDevice = New RegExp
With redNIDDevice
	.Pattern ="^(10.(118|9[4-5]|254)\.([1-2][0-9][0-9]|[1-9][0-9]|[0-9])\.([1-2][0-9][0-9]|[1-9][0-9]|[0-9]))" 
	.IgnoreCase = True
	.Global = True
End With
' Doesn't match Blue NIDs yet. 
'"10.(118|9[4-5]|254)\.([1-2][0-9][0-9]|[1-9][0-9]|[0-9])\.([1-2][0-9][0-9]|[1-9][0-9]|[0-9])"

' Covers both configured, unconfigured, and blue NIDs
Set AccedianDevice = New RegExp
With AccedianDevice
	.Pattern = "^(([Nn][Ii][Dd]-.*\.\w{3}\d)|([Gg][Mm]\d*))"
	.IgnoreCase = True
	.Global = True
End With

'ER and ES devices. Only checking for the Red TID.
Set erDevice = New RegExp
With erDevice
	.Pattern = "(\w{3}\d*[.-][eE]([rR]|[sS])\d*)"
	.IgnoreCase = True
	.Global = True
End With

'Specifically idenifies ZG and ZF devices
'Used in the Ring Validator
Set asr920Device = New RegExp
With asr920Device
	.Pattern = "(\w{8}([zZ|yY])([gG]|[fF]|[pP]|[nN]|[mM]|[eE])\d{3})"
	.IgnoreCase = True
	.Global = True
End With

'Blue Switches with 1 or 2 numbers. sw1 or sw11
Set swDevice = New RegExp
With swDevice
	.Pattern = "(sw[0-9]{1,2})\.(\w{3}[0-9])"
	.IgnoreCase = True
	.Global = True
End With

Set cnDevice = New RegExp
With cnDevice
	.Pattern = "^cn[0-9]{3}\.\w{3}[0-9]"
	.IgnoreCase = True
	.Global = True
End With

Set OrangeInterfaces = New RegExp
With OrangeInterfaces
	.Pattern = "((((Ten)?(Gigabit))|(Fast))Ethernet\d{1,2}(\/\d{1,2}){2})"
	.IgnoreCase = True
	.Global = True
End With

'Catches the word "set" or "Set" in the TID prompt
Set UserCommands = New RegExp
With UserCommands
	.Pattern = "(setup|(config[0-9]{1,2})|version|help|build|ring ? val(man)?|((((00\/)?)|([A-Za-z]{2}\/))[A-Za-z]{4}\/\d{6}\/[A-Za-z]{4})|(^\w{8}$\s*)|(test)|(6745:\d+)|^(R|RE|RED)$|^(O|OR|ORA|ORAN|ORANG|ORANGE)$|^(N\w{5})$|(madgreens)|(citadel)|(geofront))"
	.IgnoreCase = True
	.Global = True
End With

'|(^N\w{5})

Set passwordRegex = New RegExp
With passwordRegex
	.Pattern = "[Pp]assword:"
	.IgnoreCase = True
	.Global = True
End With

Set circuitIDRegex = New RegExp
' Old: ((((00\/)?)|([A-Za-z]{2}\/))[A-Za-z]{4}\/\d{6}\/[A-Za-z]{4})
With circuitIDRegex
	.Pattern = "(\w{2}\/\w{4}\/[0-9]{6}\/\w{4})|([0-9]{3}\/\w{1,4}\/\w+\/\w+)"
	.IgnoreCase = True
	.Global = True
End With

Set OrangeCLLIRegex = New RegExp
With OrangeCLLIRegex
	.Pattern = "(^\w{8}$\s*)"
	.IgnoreCase = True
	.Global = True
End With

Set MatchesAnything = New RegExp
With MatchesAnything
	.Pattern = ".*"
	.IgnoreCase = True
	.Global = True
End With

Set ThingstoStripfromUserInput = New RegExp
With ThingstoStripfromUserInput
	.Pattern = "(_\w+(\/[aA])?)"
	.IgnoreCase = True
	.Global = True
End With

Set RouteTarget = New RegExp
With RouteTarget
	.Pattern = "(6745:\d+)"
	.IgnoreCase = True
	.Global = True
End With

Set MetroRingID = New RegExp
With MetroRingID
	.Pattern = "(^(N\w{5})$)"
	.IgnoreCase = True
	.Global = True
End With

Set redBastion = New RegExp
With redBastion
	.Pattern = "(^(R|RE|RED)$)"
	.IgnoreCase = True
	.Global = True
End With

Set orangeBastion = New RegExp
With orangeBastion
	.Pattern = "(^(O|OR|ORA|ORAN|ORANG|ORANGE)$)"
	.IgnoreCase = True
	.Global = True
End With
