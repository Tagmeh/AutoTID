# AutoTID with Ring Validator Tool

### Version 3.0.2
![gitlab-cover-image](https://www.t-systems.com/image/582056/4x1/fc/1024/256/2cf56ede7b08440f991ee96343df5b2c/EA/im-bg-topic-hybrid-network-1700x425-jpg.jpg)

## Table of Contents

- [About](#about)
    - [AutoTID](#autotid)
    - [Ring Validator Tool](#ring-validator-tool)
- [Getting started](#getting-started)
    - [Option A (Download the file)](#option-a-download-the-file-directly)
    - [Option B (Create your own file)](#option-b-create-your-own-file)
    - [Create the SecureCRT Button](#creating-the-securecrt-button)
- [Settings](#settings)
- [Commands](#commands)
- [Setting up ROCi](#setting-up-roci)
- [Issues](#issues)
- [License](#license)


## About

*Note, AutoTID is designed with the Red tech in mind. AutoTID makes some assumptions about the network, like, logging out of Geofront takes you to nocsup.
Due to the fact that Nocsup requires a OTP, if you're an Orange tech reading this. It would be benefitual to log into Nocsup before running this script.
(Logging into Geofront, then Nocsup to run the script is perfectly fine.)

### AutoTID
**AutoTID is a script designed to log the user into the given network device.**


Born from my inability to remember the correct exit command of each device. 
AutoTID takes care of that problem for me. 
Give AutoTID a TID, and it'll log you out of your current device, and into the one you want, without user interaction, even if telnet is required. (All NIDs except blue are supported, but require some user interaction)

AutoTID is equiped to run pre-built search.pl searches, like for Orange circuit IDs, Orange metro ring IDs, (Orange) CLLIs, and 6745 Route targets.

There are a number of active functions built into the script, like setting a default bastion, changing your password (just in the script), 
opening and navigating the flow builder CLI for you, and the big one, the Ring Validator Tool.


### Ring Validator Tool
**Ring Validator, let the script do the ring walking for you!**


No one wants to walk a 28 node ring, just to find the last box is the one missing the vlan.
Well, the Ring Validator Tool (Ring Val) doesn't mind one bit. It's literally set and forget. 
Submit the command to autotid (ring val (caps and spaces don't matter)), give it a drop node TID and a VLAN, and let it go to town.
The tab will indicate when it's complete, and there will be a nice summary waiting for you in the CLI.

There are times where you don't need to walk an entire ring, but maybe only 5 devices. Maybe those 5 aren't even in next to each other in the ring.
The "ring val man", as in "manual", will give you a multi-line prompt (TID duplicates, random spaces, and extra characters/lines won't mess it up, because I know how to make something reslient...)
Drop a list of TIDs in there, give it a vlan at the next prompt, lean back, and enjoy the ride.

There's also an option to only search a user specified list of devices (see the [Commands](#commands) section).

- Only requires the drop node TID and a vlan to work
- Works on all known Orange drop switches (3400s, 3600s, ASR920s)
- Option to give the Ring Validator a specific list of TIDS to check (including TIDs that are a part of other rings)
- Responsive feedback in tab name
- Posts the validation output when complete
- [GIF of the Ring Validator Tool](file://///corp.global.level3.com/dfs01/sdrive-na/Training/AutoTID/Images/eNIuDke.gif)

[Return to top](#autotid-with-ring-validator-tool)
## Getting started


If your nocsup prompt is "u@h", please see the [Setting up ROCi](#setting-up-roci) section.

#### Option A (Download the file directly)
- Download the file (Scroll to the top of the page and locate the image below)

    ![](https://i.imgur.com/2X6DRCs.png)
- Unzip the folder (if zipped)
- Move the __AutoTID.vbs__ file to a safe location (somewhere you can find it later)
- Skip "Option B", and go right to the "Creating a SecureCRT button" section.

#### Option B (Copy the script manually and create the file)
- Create a new text document and remeber it's location
- The file's name doesn't matter, but it has to be .vbs. eg **AutoTID.vbs**
- go to http://gitlab/epperson.jonathan/AutoTID/raw/master/AutoTID.vbs
- Control + A, Control + S to highlight the entire document and copy it
- Now open the file you just created and paste the script and save
- Close the document, you won't need it open for the last part

#### Creating the SecureCRT button
- Create a new button in SecureCRT (Right click on the button bar > New Button)

    ![](https://imgur.com/yOwxQ2e.png) ![](https://imgur.com/7orkcDm.png)
- Select "Run Script" from the dropdown

    ![](https://imgur.com/aqevwQT.png)
    ![](https://imgur.com/xWT3ouu.png)
- Click on the ![](https://imgur.com/KpwSZqA.png) button
- Then navigate to where you saved the __AutoTID.vbs__ file
- Select it and hit "Open"

    ![](https://imgur.com/xx6N0nh.png)
- Don't forget to give the button a name

    ![](https://imgur.com/nqCudfY.png)
- Finally, hit "Ok" to create the button

    ![](https://imgur.com/oUW640e.png)
    
    
[Return to top](#autotid-with-ring-validator-tool)
## Settings

AutoTID creates a global settings file on first run (You'll run through a quick wizard).
There are also script specific settings located at the top of the script.
These can be changed to affect the behavior of individual AutoTID subprograms within the script.

For instance, you could have the AutoTID script run everything in the current tab;
Whereas, a second script, let's call it AutoTAB (A copy of AutoTID, but with different settings), opens everything in a new tab.

The only real downside is, if you run the Ring Val and are set to have it open in a new tab, 
the original tab will be tied up while Ring Val is running. It's just a quirk of SecureCRT.

![](https://imgur.com/97i5Ing.jpg)<br>
This is how my AutoTID file is setup. Everything uses the current tab (wherever the script is called from).


#### Congratulations, you're ready to make logging into network nodes look cool!



[Return to top](#autotid-with-ring-validator-tool)
## Commands

- "**password**" Prompts you to change your Orange password
- "**cuid**" Prompts you to change your CUID
- "**cuid password**" Prompts you to change your CUID password
- "**geofront**" Sets the default Orange bastion server to be Geofront
- "**citadel**" Sets the default Orange bastion server to be Citadel
- "**madgreens**" Sets the default Orange bastion server to be Madgreens
- "**setup**" Reruns the initial setup
- "**config 1-35**" Opens a new tab and runs the ADVA flow script in Geofront. Brings user to the paste prompt. Add a number to select a different Go Forward ADVA service template. The default is 1.
- Pasting in an **Orange circuit ID**, **CLLI**, **route target**, or **metro ring** will bring up, and run the corresponding search.pl search.
- "**help**" Shows current version, most commands, and contact information for the script.
- "**ring val**" Kicks off the Ring Validator portion. Paste your drop switch and the vlan and let it go!*
- "**ring val man**" Attempts to open a text box to allow the user to paste any number of TIDs (1 TID per line, ignores non-Orange-TID text) for custom ring searches.*
- "**version**" Tells you if you're up to date by getting the current script's version and the latest released script and comparing them. 

*it's recomended to open a new tab first, then run the ring validator, so as to not tie up the original tab. SecureCRT does not allow any input to the parent tab while it's running a script.


[Return to top](#autotid-with-ring-validator-tool)
## Setting up ROCi 

First you'll need to adjust your SecureCRT settings by following the below instructions. 
(Skip to Step 2 if you're not using Justin's ROCI script)

Step 1

In SecureCRT, click "File", "Connect", Right click on your favorite hostname, "Properties, "Logon Actions", and uncheck or delete the "Logon script".  

Leaving this script running has been proven to cause issues with the script and won't be needed once the ROCi account is setup.  
Note, you'll need to do this for each host that you want to use AutoTID with. (only has to be done once.)
The exact issue is that you won't be able to use the open-in-tab features of AutoTID. AutoTID will detect a logon script and run everything in the same window that it was initially run in.

Step 2

Go to Ask John and follow the instructions.

Step 3

Close out of your current session and reopen it. You should be good to go. (Don't just clone it)

[Return to top](#autotid-with-ring-validator-tool)
## Issues

Problem: When logging into an Orange device, the ssh command is sent, and two password prompts show up, but it never logs in.
Resolution: This is an issue with how the login system parses your CUID. I found that my CUID has to be lowercase in order for the password to work. Change your CUID by typing "cuid" into the prompt.

[Return to top](#autotid-with-ring-validator-tool)
## License

AutoTID is distributed under the MIT license, see the
[LICENSE](/LICENSE) for details.

[Return to top](#autotid-with-ring-validator-tool)
